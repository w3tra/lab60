import React, {Component} from 'react';

import './Messages.css';

const ENDPOINT = 'http://146.185.154.90:8000/messages';

class Messages extends Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            lastMessageTime: null,
            inputName: '',
            inputMessage: '',
            canSendMessage: false
        };
    }

    componentDidMount() {
        setInterval(() => {
            this.getMessages();
        }, 2000);
    }

    getMessages = () => {
        const url = this.state.lastMessageTime ? ENDPOINT + `?datetime=${this.state.lastMessageTime}` : ENDPOINT;
        fetch(url).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        }).then(messages => {
            let oldMessages = [...this.state.messages];
            messages.forEach(message => {
                oldMessages.push(message);
                this.setState({lastMessageTime: message.datetime})
            });
            this.setState({messages: oldMessages});
        }).catch(error => {
            console.log('There has been a problem with your fetch operation: ' + error.message);
        });
    };

    updateInput = event => {
        this.setState({[event.target.id]: event.target.value});
        this.setState({canSendMessage: this.state.inputName && this.state.inputMessage})
    };

    sendMessage = () => {
        if (this.state.inputName && this.state.inputMessage) {
            const data = new URLSearchParams();
            data.set('message', this.state.inputMessage);
            data.set('author', this.state.inputName);

            fetch(ENDPOINT, {
                method: 'post',
                body: data,
            }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Network response was not ok.');
            }).then(() => {
                this.setState({inputName: '', inputMessage: '', canSendMessage: false})
            }).catch(error => {
                console.log('There has been a problem with your fetch operation: ' + error.message);
            });
        } else {
            alert('Inputs must not be empty');
        }
    };

    render() {

        const messages = this.state.messages.map(message => {
            return <div className="Message" key={message._id}>
                <b className="Author">{message.author}</b>
                <i> says:</i>
                <span className="Message-text"> {message.message}</span>
            </div>
        });

        return (
            <div className="Messages">
                <div className="Form">
                    <input type="text" id="inputName" placeholder="Name" onChange={this.updateInput}
                           value={this.state.inputName}/>
                    <input type="text" id="inputMessage" placeholder="Message" onChange={this.updateInput}
                           value={this.state.inputMessage}/>
                    <button onClick={this.sendMessage} disabled={!this.state.canSendMessage}>Send</button>
                </div>
                <div className="Messages-box">
                    {messages}
                </div>
            </div>
        );
    }
}

export default Messages;